describe("Tickets", () => {
  beforeEach(() =>
    cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html")
  );
  it("Escrever nos textboxes", () => {
    cy.get("#first-name").type("Luana");
    cy.get("#last-name").type("Terense");
    cy.get("#email").type("luana.r.matias@gmail.com");
  });

  it("Selecionar opção do combobox", () => {
    cy.get("#ticket-quantity").select("2");
  });

  it("Selecionar um radio button", () => {
    cy.get("#vip").check();
  });

  it("Selecionar um radio button", () => {
    cy.get("#social-media").check();
  });

  it("Selecionar um radio button e desmarcar em seguida", () => {
    cy.get("#social-media").check();
    cy.get("#friend").check();
    cy.get("#social-media").uncheck();
  });

  it("Selecionar um radio button e desmarcar em seguida", () => {
    cy.get("header h1").should("contain", "TICKETBOX");
  });

  it("Verificar se o campo e-mail está vermelho", () => {
    cy.get("#email").type("teste.teste");
    cy.get("#email.invalid").should("exist");
  });

  it("Verificar se o campo e-mail por completo com alias", () => {
    cy.get("#email").as("email").type("teste.teste");

    cy.get("#email.invalid").should("exist");

    cy.get("@email").clear().type("luana.r.matias@gmail.com");

    cy.get("#email.invalid").should("not.exist");
  });

  it("End to end", () => {
    const firstName = "Luana";
    const lastName = "Terense";
    const fullName = `${firstName} ${lastName}`;

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("luana.r.matias@gmail.com");
    cy.get("#ticket-quantity").select("2");
    cy.get("#vip").check();
    cy.get("#requests").type("texto");

    cy.get(".agreement p").should(
      "contain",
      `I, ${fullName}, wish to buy 2 VIP tickets.`
    );

    cy.get("#agree").click();

    cy.get("#signature").type(fullName);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("button[type='reset']").click();

    cy.get("@submitButton").should("be.disabled");
  });

  it("Criar função do cypress", () => {
    const customer = {
        firstName: "Luana",
        lastName: "Terense",
        email: "luana.r.matias@gmail.com"
    };

    cy.preencherCamposObrigatorios(customer);

    cy.get("button[type='submit']")
    .as("submitButton")
    .should("not.be.disabled");

    cy.get("#agree").uncheck();

    cy.get("@submitButton").should("be.disabled");
  });

});